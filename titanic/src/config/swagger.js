exports.options = {
    routePrefix: '/documentation',
    exposeRoute: true,
    swagger: {
        info: {
            title: 'Titanic API',
            description: 'Building a REST API with Node.js, MongoDB, Fastify and Swagger',
            version: '1.0.0'
        },
        externalDocs: {
            url: 'https://swagger.io',
            description: 'Find more info here'
        },
        host: '127.0.0.1:8000',
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json']
    }
}