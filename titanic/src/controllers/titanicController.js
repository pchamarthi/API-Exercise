// External Dependencies
const boom = require('boom')

// Get the Model
const Titanic = require('../models/Titanic')

// Get all people
exports.getPeople = async (req, resp) => {
    try {
        const people = await Titanic.find()
        return people
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Get single person by ID
exports.getSinglePerson = async (req, resp) => {
    try {
        const id = req.params.id
        const person = await Titanic.findById(id)
        return person
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Add a new Person
exports.addPerson = async (req, resp) => {
    try {
        const person = new Titanic({...req.body})
        return person.save()
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Update an existing Person
exports.updatePerson = async (req, resp) => {
    try {
        const id = req.params.id
        const person = req.body
        const { ...updateData } = person
        const update = await Titanic.findByIdAndUpdate(id, updateData, {new: true})
        return update
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Delete a Person
exports.deletePerson = async (req, resp) => {
    try {
        const id = req.params.id
        const person = await Titanic.findByIdAndRemove(id)
        return person
    } catch (err) {
        throw boom.boomify(err)
    }
}