const mongoose = require('mongoose')

const titanicSchema = new mongoose.Schema({
    survived: Boolean,
    passengerClass: Number,
    name: String,
    sex: String,
    age: Number,
    siblingsOrSpousesAboard: Number,
    parentsOrChildrenAboard: Number,
    fare: Number
})

module.exports = mongoose.model('Titanic', titanicSchema, 'titanic')