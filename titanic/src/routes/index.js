const titanicController = require('../controllers/titanicController')

const routes = [
    {
        method: 'GET',
        url: '/people',
        handler: titanicController.getPeople
    },
    {
        method: 'GET',
        url: '/people/:id',
        handler: titanicController.getSinglePerson
    },
    {
        method: 'POST',
        url: '/people',
        handler: titanicController.addPerson
    },
    {
        method: 'PUT',
        url: '/people/:id',
        handler: titanicController.updatePerson
    },
    {
        method: 'DELETE',
        url: '/people/:id',
        handler: titanicController.deletePerson
    }
]

module.exports = routes