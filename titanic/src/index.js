// Require Fastify framework and initialized it
const fastify = require('fastify')({
    logger: true
})

// Require external modules (Mongo)
const mongoose = require('mongoose')

// Import routes
const routes = require('./routes')

// Import Swagger Options
const swagger = require('./config/swagger')

// Register Swagger
fastify.register(require('fastify-swagger'), swagger.options)

// Connect to DB
// Get Username, Password and Database name from Env variables
const mongouri = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:27017/${process.env.MONGO_DATABASE}`
// console.log(mongouri)
mongoose.connect(mongouri, { useNewUrlParser: true })
    .then(() => console.log('MongoDB connected'))
    .catch(err => console.log(err))

routes.forEach((route, index) => {
    fastify.route(route)
})

// Run the server
const start = async () => {
    try {
        await fastify.listen(process.env.APPPORT, '0.0.0.0')
        fastify.swagger()
        fastify.log.info(`server listening on port ${fastify.server.address().port}`)
    } catch (err) {
        fastify.log.error(err)
        process.exit(1)
    }
}

start()