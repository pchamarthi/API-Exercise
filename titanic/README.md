# Developing the API (titanic)

## Prerequisites

- nodejs (v8.9.4)
- MongoDB (v4)
- Docker for Desktop
- Docker-Compose
- VSCode
  - Install [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) extension

  ```(text)
    Name: REST Client
    Id: humao.rest-client
    Description: REST Client for Visual Studio Code
    Version: 0.21.2
    Publisher: Huachao Mao
    VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=humao.rest-client
  ```

## API - using nodejs, mongoose and fastify

API app was developed with nodejs - using mongoose and fastify webframework (inspired by express) for api development

- This api uses MongoDB for it's backend to store the records
- Database schema definition can be found here [src/models/Titanic.js](src/models/Titanic.js)
- Controller for this model is defined here [src/controllers/titanicController.js](src/controllers/titanicController.js)
- Routes are defined here [src/routes/index.js](src/routes/index.js)
- [index.js](src/index.js) brings it all together
- `start` script is defined in [package.json](package.json) is responsible for `npm start`

## API Backend (MongoDB Database)

### Configuring MongoDB (local install)

API will connect to MongoDB backend for querying `people` records for titanic.  We will create a separate database user and grant this db user `readWrite` access to db `titanic`

#### Creating db user

MongoDB server `mongod` starts with no authentication enabled by default.  We'll create a user named `tadmin` with password `soopersecret` with `readWrite` access to db named `titanic`.

```(shell)
mongo titanic --eval "db.createUser({user: 'tadmin', pwd: 'soopersecret', roles:[{role:'readWrite',db:'titanic'}]});"
Successfully added User
...
```

Once the user is created successfully, we can now start mongo server with authentication enabled.

```(shell)
# Shutdown mongo server
mongod --shutdown

# Start server with auth enabled
mongod --auth
```

We can now connect to the DB with these credentials

#### Importing data into the DB

To import the [titanic.csv](titanic.csv) file into a database named `titanic` we'll use `mongoimport` utility. [CSV file](titanic.csv) contains headers in the first line.   However, as the target Field names are different than the provided ones, we'll need to transform these field names whilst importing. These Field Names (transformed ones) are stored in [titanic.fieldFile](titanic.fieldFile) file.

```(shell)
$ mongoimport --type csv --file titanic.csv --fieldFile titanic.fieldFile -d titanic -c titanic -u tadmin -p soopersecret
2019-03-ddT15:32:33.537+0000    connected to: localhost
2019-03-ddT15:32:33.601+0000    imported 888 documents
```

## Running the application (without docker)

- Ensure mongodb is up and running with `--auth` enabled
- Set the following ENV variables
  - MONGO_USER=tadmin
  - MONGO_PASSWORD=soopersecret
  - MONGO_HOST=localhost
  - MONGO_DATABASE=titanic
- cd into folder `titanic`
- Install node dependencies `npm install`
- Run the app using `npm start`

```(shell)
cd titanic
npm install
npm start
```

## Config Files

In order to keep the config separate from the code, all config files are stored under [config](config) directory.
For e.g config for MongoDB can be found in [config/mongodb/mongodb.env](config/mongodb/mongodb.env) and for the api [here](config/titanic/titanic.env)

## Dockerfiles

Find below the instructions to run both MongoDB and API as individual containers.

- MongoDB docker instructions can be found [here](DBSetup.md)
- NodeJS app (api) can be containerized using this Multi-stage [Dockerfile](Dockerfile)

## Launch application using docker-compose

Both api frontend and backend can be defined as services and glued together with `networks` - [docker-compose.yml](docker-compose.yml) defines the services.  Container names are defined in [docker-compose.yml](docker-compose.yml) as `titanic_api` and `titanic_db` respectively.

Entire application can be stood up by using `docker-compose up` command - this builds the api frontend (nodejs) image as defined in the [Dockerfile](dockerfile) and runs it along with `mongo` image.

```(shell)
cd titanic
docker-compose up
```

verify if the api frontend is connected to backend successfully.  

> CAVEAT: Even though `titanicdb` service was added as a dependent service to `titanicapi` in the `docker-compose.yml` file, the `titanicdb` service takes a few seconds longer to start due to pre-configure initialisation scripts. This results in the `titanicapi` container unable to connect to the db intially.  To mitigate this situation, sleep command is used to delay the start to titanicapi container for 30s. If this timeout is not sufficient then you'll end with connection error as shown below

```(shell)
$ docker logs titanic_api

titanic_api   | { MongoNetworkError: failed to connect to server [titanicdb:27017] on first connect [MongoNetworkError: connect ECONNREFUSED 172.21.0.2:27017]
titanic_api   |     at Pool.<anonymous> (/app/node_modules/mongodb-core/lib/topologies/server.js:564:11)
titanic_api   |     at Pool.emit (events.js:182:13)
titanic_api   |     at Connection.<anonymous> (/app/node_modules/mongodb-core/lib/connection/pool.js:317:12)
titanic_api   |     at Object.onceWrapper (events.js:273:13)
titanic_api   |     at Connection.emit (events.js:182:13)
titanic_api   |     at Socket.<anonymous> (/app/node_modules/mongodb-core/lib/connection/connection.js:246:50)
titanic_api   |     at Object.onceWrapper (events.js:273:13)
titanic_api   |     at Socket.emit (events.js:182:13)
titanic_api   |     at emitErrorNT (internal/streams/destroy.js:82:8)
titanic_api   |     at emitErrorAndCloseNT (internal/streams/destroy.js:50:3)
titanic_api   |     at process._tickCallback (internal/process/next_tick.js:63:19)
titanic_api   |   name: 'MongoNetworkError',
titanic_api   |   errorLabels: [ 'TransientTransactionError' ],
```

> However, once the `titanicdb` service is fully up, the `titanicapi` service can be restarted using `docker-compose restart titanicapi` and this point the connection will succeed. In a swarm scenario, health-checks could be added to mitigate this situation.

restart the `titanicapi` docker-compose service to resolve this. You should now see output `MongoDB connected`

```(shell)
$ docker-compose restart titanicapi
Restarting titanic_api ... done
$ docker logs titanic_api

titanic_api   | {"level":30,"time":1553955674081,"pid":28,"hostname":"432c1ee95938","msg":"Server listening at http://0.0.0.0:8000","v":1}
titanic_api   | {"level":30,"time":1553955674083,"pid":28,"hostname":"432c1ee95938","msg":"server listening on port 8000","v":1}
titanic_db    | 2019-03-30T14:21:14.086+0000 I NETWORK  [listener] connection accepted from 172.21.0.3:45778 #1 (1 connection now open)
titanic_db    | 2019-03-30T14:21:14.092+0000 I NETWORK  [conn1] received client metadata from 172.21.0.3:45778 conn1: { driver: { name: "nodejs", version: "3.1.13" }, os: { type: "Linux", name: "linux", architecture: "x64", version: "4.9.125-linuxkit" }, platform: "Node.js v10.14.2, LE, mongodb-core: 3.1.11" }
titanic_db    | 2019-03-30T14:21:14.121+0000 I ACCESS   [conn1] Successfully authenticated as principal tadmin on titanic
titanic_api   | MongoDB connected
```

Now load the DB with data from [titanic.csv](titanic.csv)
Login to running mongodb container and run`mongoimport`

```(shell)
$ docker cp titanic.csv titanic_db:/
$ docker cp titanic.fieldField titanic_db:/
$ docker exec -it titanic_db bash
# mongoimport --type csv --file titanic.csv --fieldFile titanic.fieldFile -d titanic -c titanic -u tadmin -p soopersecret
2019-03-ddT15:32:33.537+0000    connected to: localhost
2019-03-ddT15:32:33.601+0000    imported 888 documents
# exit
```

At this point you can now test if the application is up by doing a curl on [http://localhost:8000/people/](http://localhost:8000/people/)

```(shell)
$ curl -i http://localhost:8000/people/
HTTP/1.1 200 OK
content-type: application/json; charset=utf-8
content-length: 175140
Date: Fri, 29 Mar 2019 14:24:32 GMT
Connection: keep-alive

....
```

### Shutting down the application

To gracefully shutdown the application, we'll use `docker-compose down` command, which

```(shell)
$ docker-compose down
topping titanic_api ... done
Stopping titanic_db  ... done
Removing titanic_api ... done
Removing titanic_db  ... done
Removing network titanic_titanic
```

## Testing API Calls using [REST Client Extension](https://github.com/Huachao/vscode-restclient)

To test the application, we'll use VSCode's [REST CLIENT](https://github.com/Huachao/vscode-restclient) extension.

All REST calls are defined in [rest-calls.http](rest-calls.http) file.

- Launch VSCode, open file [rest-calls.http](rest-calls.http)
- modify `hostname` if not using `localhost`
- Click on link `Send Request` above GET, POST, PUT and DELETE commands

> NOTE: Swagger documentation is available at http://localhost:8000/documentation

## Kubernetes (k8s)

All config values are stored as secrets in k8s.  MongoDB INITDB credential info is stored in secret named `mongosecret` and app user credentials are stored in a secret name `mongousersecret`

```(shell)
kubectl create secret generic mongosecret \
  --from-literal=mongousername=<MONGO_INITDB_USERNAME> \
  --from-literal=mongopassword=<MONGO_INITDB_PASSWORD> \
  --from-literal=mongodatabase=<MONGO_INITDB_DATABASE>

kubectl create secret generic mongousersecret \
  --from-literal=mongousername=<MONGO_APP_USERNAME> \
  --from-literal=mongopassword=<MONGO_APP_PASSWORD>
```

Azure storage account credentials are also stored as secrets (See below)

### k8s prerequisites

- Azure
  - [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) to create/manage azure resources
  - [Azure Container Registry](https://docs.microsoft.com/en-us/azure/container-registry/) (ACR) to store private images
  - Azure Storage Account

As images are stored in azure private registry we'll need to configure kubernetes to access this private registry.
A private registry can be created in azure by following the instructions [here](https://docs.microsoft.com/en-us/azure/container-registry/container-registry-get-started-azure-cli).

Create a Service Principal (SP) in Azure and grant this service principal `AcrPull` role assignment.
Note the SP ID and password. We'll need this below.

Tag the image, then login to ACR and then push the `titanic` image up to this private registry

```(shell)
$ docker tag titanic <regname.azurecr.io>/titanic:latest

# Login to acr
$ az acr login -n <regname> -u <sp-id> -p <sp-password>
Login succeeded

$ docker push <regname.azurecr.io>/titanic:latest
The push refers to repository [regname.azurecr.io/titanic]
e2b8f7fb1c13: Pushed
d7bf55d3a3a2: Pushed
c38f839994e2: Pushed
d3b4a305fd3f: Pushed
3475cae150a2: Pushed
bcf2f368fe23: Pushed
latest: digest: sha256:21d47611f51538580661b13944fca2af4477150f8b02bd33bc05dc0bc483de86 size: 1574
```

### Configuring ACR credentials in k8s

we'll need to create a `docker-registry` secret named `acrcred` with credentials of the private registry as describe above

```(shell)
$ kubectl create secret docker-registry acrcred \
  --docker-server=<FQDN of private-registry-server>
  --docker-username=<SP-ID>
  --docker-password=<SP-PASSWORD>
  --docker-email=<DOCKER_EMAIL>

secret/acrcred-66h7d4d986 created
```

Now in our pod spec we can use this secret as such

```(yaml)
apiVersion: v1
kind: Pod
metadata:
  name: titanicapi
spec:
  containers:
  - name: titanicapi
    image: <regname.azurecr.io>/titanic:latest
  imagePullSecrets:
  - name: acrcred
...
```

### Persistent Volumes (PV) in the Cloud

We'll use Azure File Share as our PV for the titanic's API backend.  
PersistentVolume, PersistentVolumeClaim spec can be found [here](titanic-pv.yaml)

#### Steps to create a storage account in Azure

create storage account and a file share named `titanicpv`

```(shell)
AKS_PERS_STORAGE_ACCOUNT_NAME=mystorageaccount$RANDOM
AKS_PERS_RESOURCE_GROUP=myAKSShare
AKS_PERS_LOCATION=eastus
AKS_PERS_SHARE_NAME=titanicpv

# Create a resource group
az group create --name $AKS_PERS_RESOURCE_GROUP --location $AKS_PERS_LOCATION

# Create a storage account
az storage account create -n $AKS_PERS_STORAGE_ACCOUNT_NAME -g $AKS_PERS_RESOURCE_GROUP -l $AKS_PERS_LOCATION --sku Standard_LRS

# Export the connection string as an environment variable, this is used when creating the Azure file share
export AZURE_STORAGE_CONNECTION_STRING=`az storage account show-connection-string -n $AKS_PERS_STORAGE_ACCOUNT_NAME -g $AKS_PERS_RESOURCE_GROUP -o tsv`

# Create the file share
az storage share create -n $AKS_PERS_SHARE_NAME

# Get storage account key
STORAGE_KEY=$(az storage account keys list --resource-group $AKS_PERS_RESOURCE_GROUP --account-name $AKS_PERS_STORAGE_ACCOUNT_NAME --query "[0].value" -o tsv)

# Echo storage account name and key
echo Storage account name: $AKS_PERS_STORAGE_ACCOUNT_NAME
echo Storage account key: $STORAGE_KEY
```

We'll now create generic `secret` called `azuresecret` to store the storage account's name and accesskey, this is needed when we use `azureFile` as our volume

```(shell)
kubectl create secret generic azuresecret \
  --from-literal=azurestorageaccountname=$AKS_PERS_STORAGE_ACCOUNT_NAME \
  --from-literal=azurestorageaccountkey=$STORAGE_KEY
```

Pod spec to use `azureFile` looks like this

```(yaml)
apiVersion: v1
kind: Pod
metadata:
  name: titanic-db
spec:
  containers:
  - image: mongo
    name: titanic-db
    volumeMounts:
      - name: azure
        mountPath: /data/db
  volumes:
  - name: azure
    azureFile:
      secretName: azure-secret
      shareName: titanicpv
      readOnly: false
```

Persistent Volume and Persisent Volume Claims are defined in [titanic-pv.yaml](k8s/titanic-pv.yaml)
Deployments are defined in [titanic-deployments.yaml](k8s/titanic-deployments.yaml)
Services are defined in [titanic-services.yaml](k8s/titanic-services.yaml)

> NOTE: Pods can be configured with initContainers in the container spec for initialization scripts and other pre-config steps, this is not refelected here as this is out of scope this exercise
