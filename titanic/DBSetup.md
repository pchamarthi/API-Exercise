# Setup and configure MongoDB (docker)

MongoDB `INITDB` params are stored in [mongodb.env](config/mongodb/mongodb.env) file. These INIT params are used to set the default root username and password.
****
Mongo initialization script to create db user named `tadmin` is defined in [mongo-init.js](config/mongodb/initdb.d/mongo-init.js).  We'll mount this file as read-only volume into the container, this script will then be executed during container start, thus adding the db user `tadmin` to the database.

As containers could exit (or die) due to resource constraints or other errors, we need to make use of a persistent data volume.
In this case, a folder named `data` is create under the folder `titanic` of this repo and this folder is then mounted to the container.
MongoDB port `27017` needs to be exposed as well.

Essentially, we run the `mongo` container as such

```(shell)
cd titanic
mkdir -p data

docker pull mongo

docker run -d --name mongo \
    -p "27017:27017" \
    --env-file $(pwd)/config/mongodb/mongodb.env \
    -v $(pwd)/config/mongodb/initdb.d/mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js:ro \
    -v $(pwd)/data:/data/db \
    mongo
```

## Import data into DB from `titanic.csv`

To import the [titanic.csv](titanic.csv) file into a database named `titanic` we'll use `mongoimport` utility.
[CSV file](titanic.csv) contains headers in the first line. However, as the target Field names are different than the provided ones, we;'ll need to transform these field names whilst importing. These Field Names (transformed ones) are stored in [titanic.fieldFile](titanic.fieldFile) file.

Before running the import we'll need to copy these two files into the container

```(shell)
docker cp titanic.csv mongo:/
docker cp titanic.fieldFile mongo:/
```

next we'll step into the container and run the `mongoimport` command

```(shell)
docker exec -it mongo bash
```

once inside the container prompt run the following

```(shell)
$ mongoimport --type csv --file titanic.csv --fieldFile titanic.fieldFile -d titanic -c titanic -u tadmin -p soopersecret
2019-03-ddT15:32:33.537+0000    connected to: localhost
2019-03-ddT15:32:33.601+0000    imported 888 documents
```

DB `titanic` is now setup with a collection named `titanic` and populated with all records from CSV