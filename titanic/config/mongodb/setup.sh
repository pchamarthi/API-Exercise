#!/bin/bash

ADMINUSER="$1"
ADMINPASS="$2"

mongod &

RET=1
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MongoDB service startup"
    sleep 5
    mongo admin --eval "help" >/dev/null 2>&1
    RET=$?
done

echo "=> Creating an admin user in MongoDB for db titanic"
mongo titanic --eval "db.createUser({user: '$ADMINUSER', pwd: '$ADMINPASS', roles:[{role:'readWrite',db:'titanic'}]});"

# if [ $? -eq 0 ]; then
#     ## Populate DB using mongoimport
#     mongoimport --type csv --file ./titanic.csv --fieldFile ./titanic.fieldFile -d titanic -c titanic -u $ADMINUSER -p $ADMINPASS
# fi

# mongod --shutdown

# exit $?